// C++ Components Demo -- Minimal demo for component based programming
//                        in C++ w/o middle ware
//
// Copyright (C) 2022 M E Leypold
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "FortuneCookie.hpp"
#include "messages.hpp"
#include <iostream>

FortuneCookie::FortuneCookie(unsigned int message_index)
  : message(messages[message_index])
{ }

std::string FortuneCookie::getMessage(){
  std::cout << "Called FortuneCookie::getMessage() in MessagePoolBasedService.so." << std::endl;  
  return message;
}
