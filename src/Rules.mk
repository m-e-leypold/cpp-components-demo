# C++ Components Demo -- Minimal demo for component based programming
#                        in C++ w/o middle ware
#
# Copyright (C) 2022 M E Leypold
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

all::
clean::

CPP-FILES = $(wildcard *.cpp)
HPP-FILES = $(wildcard *.hpp)
OBJ-FILES = $(CPP-FILES:%.cpp=%.o)
DEP-FILES = $(CPP-FILES:%.cpp=%.d)

-include $(DEP-FILES)

CXXFLAGS  =  -I ../model -MMD -fPIC

$(info OBJ-FILES = $(OBJ-FILES))

all:: $(OBJ-FILES)

clean::
	rm -f *.o *~ *.d *.so

ifdef SO-NAME

all:: $(SO-NAME).so

$(SO-NAME).so: $(OBJ-FILES)
	g++ -shared $^ -o $@

clean::
	rm -f $(SO-NAME).so
endif


ifdef EXE-NAME

all:: $(EXE-NAME).exe

$(EXE-NAME).exe: $(OBJ-FILES)
	g++ $^ -o $@

clean::
	rm -f $(EXE-NAME).exe

endif

