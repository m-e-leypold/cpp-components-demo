// C++ Components Demo -- Minimal demo for component based programming
//                        in C++ w/o middle ware
//
// Copyright (C) 2022 M E Leypold
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "CookieBaker.hpp"
#include "SecretCookie.hpp"
#include <iostream>
#include <random>


CookieBaker::CookieBaker ()  
  :
  os_random(),    
  random_generator(os_random())
{
  std::cout << "Constructing CookieBaker in SecretMessagesService.so." << std::endl;
}

unsigned int CookieBaker::random(unsigned int range){
  
  std::uniform_int_distribution<  uint_least32_t > distribution (0,range-1);
  auto n = distribution(random_generator);
  
  return n;
}

std::shared_ptr<FortuneCookieInterface> CookieBaker::getCookie(){
  std::cout << "Called CookieBaker::getCookie() in SecretMessagesService.so." << std::endl;
  
  return std::make_shared<SecretCookie>(random(1<<31));
}

std::shared_ptr<CookieBakerInterface> getCookieBakerService(){  
  std::cout << "Called getCookieBakerService() in SecretMessagesService.so." << std::endl;

  static auto instance = std::make_shared<CookieBaker>(); 
  return instance;
}
