# c++-component-demo

## About

This a demo for component based programming in C++ with no middle-ware
to support an article that will be drafted here an finally published
at <https://www.glitzersachen.de/blog/2022/components-and-services>.

The article draft and the demo implementation will be found at
https://gitlab.com/m-e-leypold/cpp-component-demo (primary location)
and https://github.com/m-e-leypold/cpp-component-demo (backup
location).

