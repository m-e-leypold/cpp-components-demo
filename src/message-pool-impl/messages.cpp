#include "messages.hpp"

// C++ Components Demo -- Minimal demo for component based programming
//                        in C++ w/o middle ware
//
// Copyright (C) 2022 M E Leypold
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


const std::size_t n_messages = 3;

const char* messages[n_messages] = {

  "Stay the patient course."
  " Of little worth is your ire,"
  " The network is down.",
  
  "Out of memory."
  " We wish to hold the whole sky,"
  " But we never will.",
  
  "Windows NT crashed."
  " I am the Blue Screen of Death."
  " No one hears your screams."  
};
