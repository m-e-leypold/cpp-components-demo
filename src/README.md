# c++-component-demo/src

## About

This a demo implementation for component based programming on C++ with
no middle-ware as described in the article drafted in the parent
directory --- see [../README.md](../README.md) --- which will be published at
<https://www.glitzersachen.de/blog/2022/components-and-services>.

The article draft and the demo implementation can be found at
https://gitlab.com/m-e-leypold/cpp-component-demo (primary location)
and https://github.com/m-e-leypold/cpp-component-demo (backup
location).

## License

The demo implementation (everything in sub-directory src/) is licensed under GPL version 3 as follows:

> Copyright (C) 2022 M E Leypold
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.

The full license text is avilable in file [LICENSE.md](LICENSE.md).
